#ifndef FIFO_H
#define FIFO_H

/**
 * @file fifo.h
 * @brief fifo function header file.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @date 2011-02-20
 */

/**
 * initialize interrupts.
 */
void init_iterrupts();
/**
 * put data to FIFO buffer.
 */
int fifo_put(struct FIFOBUF *fifo, unsigned char data);
/**
 * get data from FIFO buffer.
 */
int fifo_get(struct FIFOBUF * fifo);
/**
 * check FIFO buffer status.
 */
int fifo_status(struct FIFOBUF *fifo);
/**
 * initialize FIFO buffer.
 */
void fifo_init(struct FIFOBUF *fifo, int size, unsigned char *data);

#endif
