#ifndef INTERRUPT_H
#define INTERRUPT_H

/**
 * @file interrupt.h
 * @brief Hardware intrrupt control header file.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @date 2011-02-20
 */

/**
 * Initialize PIC.
 */
void init_pic();
/**
 * Initialize PIT.
 */
void _keyboard_interrupt();
/**
 * keyboard interrupt event.
 */
void _mouse_interrupt();
/**
 * mouse interrupt event.
 */
void _timer_interrupt();

#endif
