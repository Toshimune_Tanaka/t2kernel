/**
 * @file test.c
 * @brief test source file of fifo buffer functions.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @date 2011-02-20
 */
#include <CUnit/CUnit.h>
#include <CUnit/Console.h>
#include "fifo.h"
#include "buffer.h"

void test_init_iterrupts(void);
void test_fifo_put(void);
void test_fifo_get(void);
void test_fifo_status(void);
void test_fifo_init(void);

int main() {
  CU_pSuite suite;
  CU_initialize_registry();
  
  suite = CU_add_suite("basic functions", NULL, NULL);

  CU_add_test(suite, "test_fifo_put", test_fifo_put);
  CU_add_test(suite, "test_fifo_get", test_fifo_get);
  CU_add_test(suite, "test_fifo_status", test_fifo_status);
  CU_add_test(suite, "test_fifo_init", test_fifo_init);
  
  CU_console_run_tests();
  CU_cleanup_registry();

  return 0;
}

void test_init_iterrupts(void) {
  // I can't write test codes this function.
}

void test_fifo_put(void) {
  struct FIFOBUF keyfifo;
  char keybuf[32];
  int i;
  
  fifo_init(&keyfifo, 32, keybuf);
  
  keyfifo.free = 0;
  keyfifo.size = 1;
  keyfifo.q = 0;
  for(i = 0; i < 26; i++) {
	CU_ASSERT(fifo_put(&keyfifo, (char)(i + 65)) == -1);
  }
}

void test_fifo_get(void) {
  struct FIFOBUF keyfifo;
  char keybuf[32];
  int i = 0;

  fifo_init(&keyfifo, 32, keybuf);
  for(i = -32; i < 32; i++) {
	keyfifo.free = i;
	keyfifo.size = i;
	CU_ASSERT(fifo_get(&keyfifo) == -1);
  }

  fifo_init(&keyfifo, 32, keybuf);
  for(i = 0; i < 26; i++) {
	fifo_put(&keyfifo, (char)(i + 65));
	CU_ASSERT(fifo_get(&keyfifo) == (char)(i + 65));
  }
}

void test_fifo_status(void) {
  struct FIFOBUF keyfifo;
  char keybuf[32];
  int i = 0;
  
  fifo_init(&keyfifo, 32, keybuf);

  for(i = 0; i < 99; i++) {
	keyfifo.free = i;
	keyfifo.size = 0;
	CU_ASSERT(fifo_status(&keyfifo) == keyfifo.size - i);
  }

  for(i = 0; i < 99; i++) {
	keyfifo.free = 1;
	keyfifo.size = i;
	CU_ASSERT(fifo_status(&keyfifo) == i - keyfifo.free);
  }

  for(i = 0; i < 99; i++) {
	keyfifo.free = i;
	keyfifo.size = 1;
	CU_ASSERT(fifo_status(&keyfifo) == keyfifo.size - i);
  }
}

void test_fifo_init(void) {
  struct FIFOBUF keyfifo;
  char keybuf[32];

  fifo_init(&keyfifo, 32, keybuf);
  CU_ASSERT(keyfifo.size == 32);
  CU_ASSERT(keyfifo.data == keybuf);
  CU_ASSERT(keyfifo.free == 32);
  CU_ASSERT(keyfifo.flags == 0);
  CU_ASSERT(keyfifo.p == 0);
  CU_ASSERT(keyfifo.q == 0);
}
