#
# IPL making
# author:Toshimune Tanaka <t-toshi@j.email.ne.jp>
# create:2010.02.28
# update:2012.01.14
#
# $ as -o ipl.S ipl.o
# $ objcopy -S -O binary ipl.o ipl.bin
# $ dd if=/dev/zero of=ipl.img count=2880
# $ dd if=ipl.bin of=ipl.img conv=notrunc
# $ qemu -cpu coreduo -M pc -fda ipl.img -m 16 -boot a
#

BIN_QEMU		= qemu-system-i386
LD				= ld
DD				= dd
OBJCOPY			= objcopy
AUTODOC			= doxygen

QEMU_SIZE_MEM	= 32
QEMU_MC_TYPE	= pc
# 486,pentium,pentium2,pentium3,athlon,n270,coreduo,qemu32
QEMU_CPU_TYPE	= pentium3
QEMU_SEQ_BOOT	= a

TARGET_IMG		= ipl.img
TARGET_BIN		= ipl.bin
#FILE_IMAGE      = rootfs_ext2.hdd
FILE_IMAGE      = rootfs_fat32.hdd
C_SOURCES		= fat.c ext2.c driver_ata.c timer.c kernel.c mem.c video.c interrupt.c fifo.c descriptor.c task_shell.c buffer.c
C_HEADERS		= fat.h ext2.h driver_ata.h timer.h mem.h video.h i386.h types.h interrupt.h fifo.h descriptor.h task_shell.h buffer.h
C_TARGET_OBJS	= ${C_SOURCES:.c=.o}
A_SOURCES		= ipl.S setup.S
A_TARGET_OBJS	= ${A_SOURCES:.S=.o}

all: $(TARGET_BIN) $(TARGET_IMG)
	etags *.[ch]
	$(DD) if=$(TARGET_BIN) of=$(TARGET_IMG) conv=notrunc
	$(BIN_QEMU) -cpu $(QEMU_CPU_TYPE) -M $(QEMU_MC_TYPE) --fda $(TARGET_IMG) --hda $(FILE_IMAGE) -m $(QEMU_SIZE_MEM) --boot $(QEMU_SEQ_BOOT)

$(TARGET_IMG): $(TARGET_BIN)
	$(DD) if=/dev/zero of=$@ count=2880

$(TARGET_BIN): $(A_TARGET_OBJS) $(C_TARGET_OBJS)
	$(LD) -T ipl.ls -o $@ $(A_TARGET_OBJS) $(C_TARGET_OBJS)
	$(OBJCOPY) -S -O binary $@ $@

kernel.o:
	$(CC) -o $@ -c kernel.c

task_shell.o:
	$(CC) -o $@ -c task_shell.c

fifo.o:
	$(CC) -o $@ -c fifo.c

interrupt.o:
	$(CC) -o $@ -c interrupt.c

ext2.o:
	$(CC) -o $@ -c ext2.c

driver_ata.o:
	$(CC) -o $@ -c driver_ata.c

timer.o:
	$(CC) -o $@ -c timer.c

video.o:
	$(CC) -o $@ -c video.c

mem.o:
	$(CC) -o $@ -c mem.c

descriptor.o:
	$(CC) -o $@ -c descriptor.c

setup.o:
	$(AS) -o $@ setup.S

ipl.o:
	$(AS) -o $@ ipl.S

documents: Doxyfile
	$(AUTODOC)

unittests: test_fifo.c
	gcc fifo.c test_fifo.c -Wall -L/usr/local/lib -lcunit -o unittest_fifo.bin
	gcc video.c test_video.c -Wall -L/usr/local/lib -lcunit -o unittest_video.bin

clean:
	@$(RM) *.img
	@$(RM) *.bin
	@$(RM) *.o
	@$(RM) -rf documents
	@$(RM) *.gcov
	@$(RM) *.gcno
