#ifndef TASK_SHELL_H
#define TASK_SHELL_H

/**
 * @file task_shell.h
 * @brief shell function task header file.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @date Sun 03-07-2011
 */

/**
 * shell function task.
 */
void shell_main(void);

#endif
