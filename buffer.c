/**
 * @file buffer.c
 * @brief define some buffers and functions.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @date 2011-02-20
 */
#include "buffer.h"

/**
 * @brief this function initializes command buffer.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @param -
 * @return 0:Normal termination
 */
int init_buffer_command() {
  int i;
  for(i = 0; i < MAX_LENGTH_COMMAND; i++) {
	buffer_command.data[i] = 0x00;
  }
  buffer_command.index = 0;
  
  return 0;
}

/**
 * @brief this function pushes command line strings for command buffer.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @param int keycode:
 * @param char c:
 * @return 0:Normal termination
 * @return -1:Abnormal termination
 * @return -2:Abnormal termination
 */
int push_buffer_command(int keycode, char c) {
  if((keycode != 0x2a) && (keycode != 0x36)) {
	if(MAX_LENGTH_COMMAND > buffer_command.index) {
	  buffer_command.data[buffer_command.index] = c;
	  buffer_command.index++;
	} else {
	  return -1;
	}
  } else {
	return -2;
  }
  
  return 0;
}

/**
 * @brief this function pops command line string from command buffer.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @param -
 * @return 0:Normal termination
 * @return -1:Abnormal termination
 */
int pop_buffer_command() {
  if(MAX_LENGTH_COMMAND > buffer_command.index) {
	buffer_command.data[buffer_command.index] = 0x00;
	buffer_command.index--;
  } else {
	return -1;
  }
  
  return 0;
}

/**
 * @brief this function shows from command buffer to console.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @param -
 * @return 0:Normal termination
 * @return -1:Abnormal termination
 */
int echo_command() {
  unsigned int i = 0;
  for(i = 0; i < buffer_command.index; i++) {
	if(MAX_LENGTH_COMMAND > buffer_command.index) {
	  outputchar(buffer_command.data[i]);
	} else {
	  return -1;
	}
  }
  
  return 0;
}
