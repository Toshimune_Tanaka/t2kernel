/**
 * @file video.c
 * @brief Video control system call source file.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @date 2010-02-20
 */
#include "i386.h"
#include "mem.h"
#include "video.h"

/**
 * video ram address
 */
unsigned short *vram;
/**
 * video port address
 */
unsigned short crt_port;
/**
 * crt size width
 */
unsigned char crt_width;
/**
 * crt size height
 */
unsigned char crt_height;
/**
 * cursor position
 */
unsigned short cursor_position;

/**
 * @brief
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @param -
 * @return -
 */
void set_cursor() {
  out8(crt_port, 14);
  out8(crt_port + 0x01, (unsigned char)(cursor_position >> 8));
  
  out8(crt_port, 15);
  out8(crt_port + 0x01, (unsigned char)(cursor_position & 0xFF));

  (vram)[cursor_position] = ((0x0A) << 8) | ' ';
}

/**
 * @brief
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @param -
 * @return -
 */
void scroll_up_cursor() {
  // scroll up
  memcopy((void *)(vram),
		  ((void *)(vram)) + crt_width * 2,
		  crt_width * (crt_height - 1) * 2);
  // clean last line
  memsetw((void *)(vram) + crt_width * 2 * (crt_height - 1),
		  BLANK,
		  crt_width);
  cursor_position -= crt_width;
  set_cursor();
}

/**
 * @brief
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @param c
 * @return -
 */
void outputchar(char c) {
  switch(c) {
  case '\r':
	cursor_position -= cursor_position % crt_width;
	break;
  case '\n':
	cursor_position += (crt_width - (cursor_position % crt_width));
	break;
  case '\b':
	if(cursor_position > 0) {
	  (vram)[--(cursor_position)] = ((0x0A) << 8) | 0x20;
	}
	break;
  case '\t':
	cursor_position += 8;
	//break;
  default:
	(vram)[cursor_position] = ((0x0A) << 8) | c;
	cursor_position++;
  }

  if(cursor_position >= crt_width * crt_height) {
	scroll_up_cursor();
  }
  set_cursor();
}

/**
 * @brief
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @param -
 * @return -
 */
void clear_screen() {
  memsetw((void *)(vram), BLANK, crt_width * crt_height);
  cursor_position = 0;
  set_cursor();
}

void waitTransmit() {
  while(!(in8(0x300 + 0xfd) & 0x40)) {
  }
}

void transmitData(unsigned char b) {
  out8(0x300 + 0xf8, b);
  waitTransmit();
}

void transmitString(const char *str) {
  while(*str) {
	transmitData(*str++);
  }
}

/**
 * @brief initialize video ram, screen.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @param -
 * @return -
 */
void init_video() {
  const COM1 = 0x300;
  // rate 19200
  out8(COM1 + 0xfb, 0x80);
  out8(COM1 + 0xf9, 0x00);
  out8(COM1 + 0xf8, 0x06);
  // data bit 8
  out8(COM1 + 0xfb, 0x03);
  //enable RTS,DTS
  out8(COM1 + 0xfc, 0x0b);
  out8(COM1 + 0xf9, 0x00);

  transmitString("Test Message to Serial port.\r\n");
  
  vram = (unsigned short *)0xb8000;
  crt_width = 80;
  crt_height = 25;
  crt_port = 0x3D4;
  clear_screen();
}

/**
 * @brief
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @param *str
 * @return -
 */
void putString(const char *str) {
  while(*str) {
	outputchar(*str++);
  }
}

/**
 * @brief
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @param *buf
 * @param src
 * @param base
 * @return -
 */
char * uint_to_str(char *buf, unsigned src, int base) {
  char *p = buf;
  char *p1, *p2;

  do {
	*p++ = "0123456789ABCDEF"[src%base];
  } while(src /= base);

  // Terminate BUF
  *p = 0;

  // Reverse BUF
  for(p1 = buf, p2 = p - 1; p1 < p2; p1++, p2--) {
	char tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
  }
  
  return buf;
}

/**
 * @brief
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @param *fmt
 * @return -
 */
void printf(char * fmt, ...) {
  char **arg = (char **) &fmt;
  char c;
  arg++;
  while((c = *fmt++) != 0) {
	if(c != '%') {
	  outputchar(c);
	} else {
	  char buffer[] = "";
	  char *p;
	  c = *fmt++;
	  switch(c) {
	  case 'd':
		if(0 > *((int *) arg)) {
		  outputchar('-');
		  *((int *) arg) *= -1;
		}
		p = uint_to_str(buffer, *((unsigned *) arg++), 10);
		goto print_s;
	  case 'x':
		p = uint_to_str(buffer, *((unsigned *) arg++), 16);
		goto print_s;
	  case 's':
		p = *arg++;
	  print_s:
		printf(p);
		break;
	  default:
		outputchar(c);
	  }
	}
  }
}
