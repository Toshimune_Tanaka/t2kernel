/**
 * @file ext2.c
 * @brief This source is EXT2 file system.
 * @author Toshimune Tanaka <t-toshi@j.email.ne.jp>
 * @date Sun, 15 Jan, 2012
 */

#include <string.h>
#include "video.h"
#include "driver_ata.h"
#include "ext2.h"

/**
 * @brief 
 * @date Tue, 3 Jan, 2012
 * @param 
 * @return unsigned long
 */
unsigned long get_file_size() {
}

/**
 * @brief 
 * @date Tue, 3 Jan, 2012
 * @param 
 * @return void
 */
void *map2memory_fileimage(unsigned long size) {
}

/**
 * @brief 
 * @date Mon, 9 Jan, 2012
 * @param 
 * @return 0:Success other:Error code
 */
int read_superblock(ext2_superblock *superblock) {
  u_int16_t sector[SECTOR_SIZE * 4];
  int ret = -1;

  if(read_sector(0, sector, 0x02, 4) != 0) {
	ret = -1;
	printf("Failer to read sector.\n");
  } else {
	ret = 0;
  }
  memcpy(superblock, sector, sizeof(struct ext2_superblock));

  return ret;
}

/**
 * @brief 
 * @date Sat, 15 Jan, 2012
 * @param unsigned long offset
 * @param unsigned long offset
 * @return int 0:Success Other:Error code
 */
int read_block_group(unsigned long offset) {
  u_int16_t sector[SECTOR_SIZE];
  ext2_blockgroup *blockgroup;
  int ret = -1;
  int i;

  if(read_sector(0, sector, offset, 1) != 0) {
	ret = -1;
	printf("Failer to read sector.\n");
  } else {
	ret = 0;
  }

  for(i = 0; i < SECTOR_SIZE; i++) {
  	printf("%x ", sector[i]);
  }
  printf("\n");

  memcpy(blockgroup, sector, sizeof(struct ext2_blockgroup));

  printf("---------------------\n");
  printf("     block_bitmap:%d \n", blockgroup->block_bitmap);
  printf("     inode_bitmap:%d \n", blockgroup->inode_bitmap);
  printf("      inode_table:%d \n", blockgroup->inode_table);
  printf("blocks_count_free:%d \n", blockgroup->blocks_count_free);
  printf("inodes_count_free:%d \n", blockgroup->inodes_count_free);
  printf("  used_dirs_count:%d \n", blockgroup->used_dirs_count);
  printf("              pad:%d \n", blockgroup->pad);
  
  return ret;
}

/**
 * @brief 
 * @date Tue, 3 Jan, 2012
 * @param 
 * @return unsigned long
 */
unsigned long get_block_data_address(ext2_superblock *sb,
									 ext2_blockgroup *bg) {
}

/**
 * @brief 
 * @date Tue, 3 Jan, 2012
 * @param 
 * @return u_int32_t
 */
u_int32_t blockid2address(ext2_superblock *sb,
						  u_int32_t id) {
}

/**
 * @brief 
 * @date Tue, 3 Jan, 2012
 * @param 
 * @return int
 */
int get_all_directories(dentry_list *head,
						unsigned long address,
						ext2_blockgroup *block_group) {
}

/**
 * @brief 
 * @date Tue, 3 Jan, 2012
 * @param 
 * @return u_int16_t
 */
u_int16_t read_dentry_rec_len(unsigned long address,
							  unsigned long offset) {
}

/**
 * @brief 
 * @date Tue, 3 Jan, 2012
 * @param 
 * @return void
 */
void read_dentry(ext2_dentry *dentry,
				 unsigned address,
				 unsigned long offset,
				 u_int16_t rec_len) {
}

/**
 * @brief 
 * @date Sun, 14 Oct, 2012
 * @param 
 * @return int
 */
int open(const char *file, int flags, int mode) {
  return 0;
}

/**
 * @brief 
 * @date Sun, 14 Oct, 2012
 * @param 
 * @return int
 */
int close(int fd) {
  return 0;
}

/**
 * @brief 
 * @date Sun, 14 Oct, 2012
 * @param 
 * @return int
 */
int read(int fd, void *buffer, size_t len) {
  return 0;
}

/**
 * @brief 
 * @date Sun, 14 Oct, 2012
 * @param 
 * @return int
 */
int write(int fd, const void *buffer, size_t len) {
  return 0;
}

/**
 * @brief 
 * @date Sun, 14 Oct, 2012
 * @param 
 * @return int
 */
int rename(const char *old, const char *new) {
  return 0;
}

/**
 * @brief 
 * @date Sun, 14 Oct, 2012
 * @param 
 * @return int
 */
int unlink(const char *path) {
  return 0;
}

/**
 * @brief 
 * @date Sun, 14 Oct, 2012
 * @param 
 * @return int
 */
int mkdir(const char *path, mode_t mode) {
  return 0;
}

/**
 * @brief 
 * @date Sun, 14 Oct, 2012
 * @param 
 * @return int
 */
int rmdir(const char *path) {
  return 0;
}
