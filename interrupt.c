/**
 * @file interrupt.c
 * @brief Hardware intrrupt control source file.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @date 2011-02-20
 */
#include "i386.h"
#include "video.h"
#include "buffer.h"
#include "fifo.h"
#include "mem.h"
#include "interrupt.h"

#define PIT_CTRL 0x0043
#define PIT_CNT0 0x0040
#define TIMER_FLAGS_ALLOC 1
#define TIMER_FLAGS_USING 2

/**
 * @brief Initialize PIC.
 * @param -
 * @return -
 */
void init_pic() {
  // reprogram PCI
  // initialize Command Word
  out8(0x21, 0xff);
  out8(0xa1, 0xff);
  // ICW1 : initialization for master & slave
  out8(0x20, 0x11);
  out8(0xa0, 0x11);
  // ICW2 : specify interrupt numbers
  out8(0x21, 0x20);  // PIC1 = 0x20-0x27
  out8(0xa1, 0x28);  // PIC2 = 0x28-0x2F
  // ICW3 : set pin wired to master/slave
  out8(0x21, 1 << 2);
  out8(0xa1, 0x02);
  // ICW4 : set additional option
  out8(0x21, 0x01);
  out8(0xa1, 0x01);
  // Mask interrupts
  out8(0x21, 0xfd);
  out8(0xa1, 0xff);

  return;
}

/**
 * @brief Initialize PIT.
 * @param -
 * @return -
 */
void init_pit(void) {
  int i;
  
  out8(PIT_CTRL, 0x34);
  out8(PIT_CNT0, 0x9c);
  out8(PIT_CNT0, 0x2e);

  timercontrol.count = 0;
  timercontrol.next = 0xffffffff;
  timercontrol.using = 0;
  for(i = 0; i < MAX_TIMER; i++) {
	timercontrol.timers0[i].flags = 0;
  }
  
  return;
}

/**
 * @brief keyboard interrupt event.
 * @param -
 * @return -
 */
void _keyboard_interrupt() {
  unsigned char data;
  data = 0x00;
  data = in8(0x60);
  fifo_put(&keyfifo, data);
  
  return;
}

/**
 * @brief mouse interrupt event.
 * @param -
 * @return -
 */
void _mouse_interrupt() {
  /**
  unsigned char data;
  data = 0x00;
  data = in8(0x60);
  fifo_put(&keyfifo, data);
  */
  return;
}

/**
 * @brief timer interrupt event.
 * @param -
 * @return -
 */
void _timer_interrupt() {
  int i,j;
  out8(0x0020, 0x60);
  timercontrol.count++;
  if(timercontrol.next > timercontrol.count) {
	return;
  }
  for(i = 0; i < timercontrol.using; i++) {
	if(timercontrol.timers[i]->timeout > timercontrol.count) {
	  break;
	}
	timercontrol.timers[i]->flags = TIMER_FLAGS_ALLOC;
	fifo_put(timercontrol.timers[i]->fifo,
			 timercontrol.timers[i]->data);
  }

  timercontrol.using -= 1;
  timercontrol.next = 0xffffffff;
  for(j = 0; j < timercontrol.using; j++) {
	timercontrol.timers[j] = timercontrol.timers[i + j];
  }
  if(timercontrol.using > 0) {
	timercontrol.next = timercontrol.timers[0]->timeout;
  } else {
	timercontrol.next = 0xffffffff;
  }
  
  return;
}

/**
 * @brief allocate timer parameter.
 * @param -
 * @return TIER
 */
struct TIMER *timer_alloc(void) {
  int i;
  for(i = 0; i < MAX_TIMER; i++) {
	if(timercontrol.timers0[i].flags == 0) {
	  timercontrol.timers0[i].flags = TIMER_FLAGS_ALLOC;
	  return &timercontrol.timers0[i];
	}
  }
  
  return 0;
}

/**
 * @brief free timer parameter.
 * @param TIMER
 * @return -
 */
void timer_free(struct TIMER *timer) {
  timer->flags = 0;
  
  return;
}

/**
 * @brief initialize timer
 * @param TIMER
 * @param FIFOBUF
 * @param data
 * @return -
 */
void timer_init(struct TIMER *timer,
				struct FIFOBUF *fifo,
				unsigned char data) {
  timer->fifo = fifo;
  timer->data = data;
  
  return;
}

/**
 * @brief set time timer function.
 * @param TIMER
 * @param timeout
 * @return -
 */
void timer_settime(struct TIMER *timer, unsigned int timeout) {
  int e,i,j;
  timer->timeout = timeout + timercontrol.count;
  timer->flags = TIMER_FLAGS_USING;

  e = load_eflags();
  cli();
  
  for(i = 0; i < timercontrol.using; i++) {
	if(timercontrol.timers[i]->timeout >= timer->timeout) {
	  break;
	}
  }

  for(j = timercontrol.using; j > i; j--) {
	timercontrol.timers[j] = timercontrol.timers[j-1];
  }
  timercontrol.using++;
  timercontrol.timers[i] = timer;
  timercontrol.next = timercontrol.timers[0]->timeout;
  store_eflags(e);
  
  return;
}
