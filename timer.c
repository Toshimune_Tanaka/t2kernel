/**
 * @file timer.c
 * @brief This source is driver of ATA device controller.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @date Fri, 23 Dec, 2011
 */

#include "timer.h"

volatile u_int64_t counter = 0;

/**
 * @brief wait sec.
 * @date Fri, 23 Dec, 2011
 * @param void
 * @return int 0:Success other:Error code
 */
void wait_sec(u_int32_t usec) {
  u_int64_t old = counter;
  u_int64_t end = old + (usec / 100);
  
  while(counter < end) {
	;
  }
}

/**
 * @brief wait usec.
 * @date Fri, 23 Dec, 2011
 * @param void
 * @return int 0:Success other:Error code
 */
void wait_usec(u_int32_t usec) {
  u_int64_t old = counter;
  u_int64_t end = old + (usec / 1000);
  
  while(counter < end) {
	;
  }
}
