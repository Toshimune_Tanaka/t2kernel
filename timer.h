#ifndef TIMER_H
#define TIMER_H

/**
 * @file timer.h
 * @brief **********
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @date Fri, 23 Dec, 2011
 */

#include <sys/types.h>

void wait_sec(u_int32_t usec);
void wait_usec(u_int32_t usec);

#endif
