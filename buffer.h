#ifndef BUFFER_H
#define BUFFER_H

/**
 * @file buffer.h
 * @brief define some buffers.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @date 2011-02-20
 */

#define MAX_TIMER 500
#define MAX_LENGTH_COMMAND 254

/**
 * define fifo buffer.
 */
struct FIFOBUF {
  unsigned char *data;
  int p;
  int q;
  int size;
  int free;
  int flags;
};

/**
 * define timer structure.
 */
struct TIMER {
  unsigned int timeout;
  unsigned int flags;
  struct FIFOBUF *fifo;
  unsigned char data;
};

/**
 * define timer controler.
 */
struct TIMERCTL {
  unsigned int count;
  unsigned int next;
  unsigned int using;
  struct TIMER *timers[MAX_TIMER];
  struct TIMER timers0[MAX_TIMER];
};

/**
 * define command line buffer.
 */
struct BUFFER_COMMAND {
  char data[MAX_LENGTH_COMMAND];
  unsigned int index;
};

/**
 * allocate keyboard fifo buffer.
 */
struct FIFOBUF keyfifo;

/**
 * allocate timer controler.
 */
struct TIMERCTL timercontrol;

/**
 * allocate command line buffer.
 */
struct BUFFER_COMMAND buffer_command;

/**
 * define keyboard code, non shift.
 */
static const char* KeyTable_NoShift[0xD8] = {
  "",     // 00h
  "",     // 01h [esc]
  "1",    // 02h
  "2",    // 03h
  "3",    // 04h
  "4",    // 05h
  "5",    // 06h
  "6",    // 07h
  "7",    // 08h
  "8",    // 09h
  "9",    // 0Ah
  "0",    // 0Bh
  "-",    // 0Ch
  "^",    // 0Dh
  "",     // 0Eh [backspace]
  "",     // 0Fh [tab]
  "q",    // 10h 
  "w",    // 11h
  "e",    // 12h
  "r",    // 13h
  "t",    // 14h
  "y",    // 15h
  "u",    // 16h
  "i",    // 17h
  "o",    // 18h
  "p",    // 19h
  "@",    // 1Ah
  "[",    // 1Bh
  "\n",   // 1Ch [enter]
  "",     // 1Dh [ctrl]
  "a",    // 1Eh
  "s",    // 1Fh
  "d",    // 20h
  "f",    // 21h
  "g",    // 22h
  "h",    // 23h
  "j",    // 24h
  "k",    // 25h
  "l",    // 26h
  ";",    // 27h
  ":",    // 28h
  "",     // 29h
  "",     // 2Ah
  "]",    // 2Bh
  "z",    // 2Ch
  "x",    // 2Dh
  "c",    // 2Eh
  "v",    // 2Fh
  "b",    // 30h
  "n",    // 31h
  "m",    // 32h
  "\,",   // 33h
  ".",    // 34h
  "/",    // 35h
  "",     // 36h
  "*",    // 37h
  "",     // 38h
  " ",    // 39h
  "",     // 3Ah
  "",     // 3Bh
  "",     // 3Ch
  "",     // 3Dh
  "",     // 3Eh
  "",     // 3Fh
  "",     // 40h
  "",     // 41h
  "",     // 42h
  "",     // 43h
  "",     // 44h
  "",     // 45h
  "",     // 46h
  "7",    // 47h
  "8",    // 48h
  "9",    // 49h
  "-",    // 4Ah
  "4",    // 4Bh
  "5",    // 4Ch
  "6",    // 4Dh
  "+",    // 4Eh
  "1",    // 4Fh
  "2",    // 50h
  "3",    // 51h
  "0",    // 52h
  ".",    // 53h
  "",     // 54h
  "",     // 55h
  "",     // 56h
  "",     // 57h
  "",     // 58h
  "",     // 59h
  "",     // 5Ah
  "",     // 5Bh
  "",     // 5Ch
  "",     // 5Dh
  "",     // 5Eh
  "",     // 5Fh
  "",     // 60h
  "",     // 61h
  "",     // 62h
  "",     // 63h
  "",     // 64h
  "",     // 65h
  "",     // 66h
  "",     // 67h
  "",     // 68h
  "",     // 69h
  "",     // 6Ah
  "",     // 6Bh
  "",     // 6Ch
  "",     // 6Dh
  "",     // 6Eh
  "",     // 6Fh
  "",     // 70h 
  "",     // 71h
  "",     // 72h
  "",     // 73h
  "",     // 74h
  "",     // 75h
  "",     // 76h
  "",     // 77h
  "",     // 78h
  "",     // 79h
  "",     // 7Ah
  "",     // 7Bh
  "",     // 7Ch
  "\\",   // 7Dh
  "",     // 7Eh
  "",     // 7Fh
  "",     // 80h
  "",     // 81h
  "1",    // 82h
  "2",    // 83h
  "3",    // 84h
  "4",    // 85h
  "5",    // 86h
  "6",    // 87h
  "7",    // 88h
  "8",    // 89h
  "9",    // 8Ah
  "0",    // 8Bh
  "-",    // 8Ch
  "^",    // 8Dh
  "",     // 8Eh [backspace]
  "",     // 8Fh [tab]
  "q",    // 90h
  "w",    // 91h
  "e",    // 92h
  "r",    // 93h
  "t",    // 94h
  "y",    // 95h
  "u",    // 96h
  "i",    // 97h
  "o",    // 98h
  "p",    // 99h
  "@",    // 9Ah
  "[",    // 9Bh
  "",     // 9Ch [enter]
  "",     // 9Dh [ctrl]
  "a",    // 9Eh
  "s",    // 9Fh
  "d",    // A0h
  "f",    // A1h
  "g",    // A2h
  "h",    // A3h
  "j",    // A4h
  "k",    // A5h
  "l",    // A6h
  ";",    // A7h
  ":",    // A8h
  "",     // A9h
  "",     // AAh
  "]",    // ABh
  "z",    // ACh
  "x",    // ADh
  "c",    // AEh
  "v",    // AFh
  "b",    // B0h
  "n",    // B1h
  "m",    // B2h
  ",",    // B3h
  ".",    // B4h
  "/",    // B5h
  "",     // B6h [right]
  "",     // B7h [grey]
  "",     // B8h [alt]
  "",     // B9h [spacebar]
  "",     // BAh [capslock]
  "",     // BBh [f1]
  "",     // BCh [f2]
  "",     // BDh [f3]
  "",     // BEh [f4]
  "",     // BFh [f5]
  "",     // C0h [f6]
  "",     // C1h [f7]
  "",     // C2h [f8]
  "",     // C3h [f9]
  "",     // C4h [f10]
  "",     // C5h [numlock]
  "",     // C6h [scrolllock]
  "",     // C7h [home]
  "",     // C8h [upallow]
  "",     // C9h [pgup]
  "",     // CAh [grey]
  "",     // CBh [leftallow]
  "",     // CCh [keypad]
  "",     // CDh [rightallow]
  "",     // CEh [grey+]
  "",     // CFh [end]
  "",     // D0h [downallow]
  "",     // D1h [pgdn]
  "",     // D2h [ins]
  "",     // D3h [del]
  "",     // D4h [sysreq]
  "",     // D5h [kbd]
  "",     // D6h [left]
  "",     // D7h [f11]
  "",     // D8h [f12]
};

/**
 * define keyboard code, on shift.
 */
static const char* KeyTable_Shift[0xD8] = {
  "",     // 00h
  "",     // 01h [esc]
  "!",    // 02h
  "\"",   // 03h
  "#",    // 04h
  "$",    // 05h
  "\%%",  // 06h
  "&",    // 07h
  "\'",   // 08h
  "(",    // 09h
  ")",    // 0Ah
  "~",    // 0Bh
  "=",    // 0Ch
  "~",    // 0Dh
  "" ,    // 0Eh [backspace]
  "",     // 0Fh [tab]
  "Q",    // 10h 
  "W",    // 11h
  "E",    // 12h
  "R",    // 13h
  "T",    // 14h
  "Y",    // 15h
  "U",    // 16h
  "I",    // 17h
  "O",    // 18h
  "P",    // 19h
  "`",    // 1Ah
  "{",    // 1Bh
  "",     // 1Ch [enter]
  "",     // 1Dh [ctrl]
  "A",    // 1Eh
  "S",    // 1Fh
  "D",    // 20h
  "F",    // 21h
  "G",    // 22h
  "H",    // 23h
  "J",    // 24h
  "K",    // 25h
  "L",    // 26h
  "+",    // 27h
  "*",    // 28h
  "",     // 29h
  "",     // 2Ah
  "}",    // 2Bh
  "Z",    // 2Ch
  "X",    // 2Dh
  "C",    // 2Eh
  "V",    // 2Fh
  "B",    // 30h
  "N",    // 31h
  "M",    // 32h
  "<",    // 33h
  ">",    // 34h
  "?",    // 35h
  "",     // 36h
  "_",    // 37h
  "",     // 38h
  " ",    // 39h
  "",     // 3Ah
  "",     // 3Bh
  "",     // 3Ch
  "",     // 3Dh
  "",     // 3Eh
  "",     // 3Fh
  "",     // 40h
  "",     // 41h
  "",     // 42h
  "",     // 43h
  "",     // 44h
  "",     // 45h
  "",     // 46h
  "7",    // 47h
  "8",    // 48h
  "9",    // 49h
  "-",    // 4Ah
  "4",    // 4Bh
  "5",    // 4Ch
  "6",    // 4Dh
  "+",    // 4Eh
  "1",    // 4Fh
  "2",    // 50h
  "3",    // 51h
  "0",    // 52h
  ".",    // 53h
  "",     // 54h
  "",     // 55h
  "",     // 56h
  "",     // 57h
  "",     // 58h
  "",     // 59h
  "",     // 5Ah
  "",     // 5Bh
  "",     // 5Ch
  "",     // 5Dh
  "",     // 5Eh
  "",     // 5Fh
  "",     // 60h
  "",     // 61h
  "",     // 62h
  "",     // 63h
  "",     // 64h
  "",     // 65h
  "",     // 66h
  "",     // 67h
  "",     // 68h
  "",     // 69h
  "",     // 6Ah
  "",     // 6Bh
  "",     // 6Ch
  "",     // 6Dh
  "",     // 6Eh
  "",     // 6Fh
  "",     // 70h 
  "",     // 71h
  "",     // 72h
  "",     // 73h
  "",     // 74h
  "",     // 75h
  "",     // 76h
  "",     // 77h
  "",     // 78h
  "",     // 79h
  "",     // 7Ah
  "",     // 7Bh
  "",     // 7Ch
  "|",    // 7Dh
  "",     // 7Eh
  "",     // 7Fh
  "",     // 80h
  "",     // 81h
  "1",    // 82h
  "2",    // 83h
  "3",    // 84h
  "4",    // 85h
  "5",    // 86h
  "6",    // 87h
  "7",    // 88h
  "8",    // 89h
  "9",    // 8Ah
  "0",    // 8Bh
  "-",    // 8Ch
  "^",    // 8Dh
  "",     // 8Eh [backspace]
  "",     // 8Fh [tab]
  "q",    // 90h
  "w",    // 91h
  "e",    // 92h
  "r",    // 93h
  "t",    // 94h
  "y",    // 95h
  "u",    // 96h
  "i",    // 97h
  "o",    // 98h
  "p",    // 99h
  "@",    // 9Ah
  "[",    // 9Bh
  "",     // 9Ch [enter]
  "",     // 9Dh [ctrl]
  "a",    // 9Eh
  "s",    // 9Fh
  "d",    // A0h
  "f",    // A1h
  "g",    // A2h
  "h",    // A3h
  "j",    // A4h
  "k",    // A5h
  "l",    // A6h
  ";",    // A7h
  ":",    // A8h
  "",     // A9h
  "",     // AAh
  "]",    // ABh
  "z",    // ACh
  "x",    // ADh
  "c",    // AEh
  "v",    // AFh
  "b",    // B0h
  "n",    // B1h
  "m",    // B2h
  ",",    // B3h
  ".",    // B4h
  "/",    // B5h
  "",     // B6h [right]
  "",     // B7h [grey]
  "",     // B8h [alt]
  "",     // B9h [spacebar]
  "",     // BAh [capslock]
  "",     // BBh [f1]
  "",     // BCh [f2]
  "",     // BDh [f3]
  "",     // BEh [f4]
  "",     // BFh [f5]
  "",     // C0h [f6]
  "",     // C1h [f7]
  "",     // C2h [f8]
  "",     // C3h [f9]
  "",     // C4h [f10]
  "",     // C5h [numlock]
  "",     // C6h [scrolllock]
  "",     // C7h [home]
  "",     // C8h [upallow]
  "",     // C9h [pgup]
  "",     // CAh [grey]
  "",     // CBh [leftallow]
  "",     // CCh [keypad]
  "",     // CDh [rightallow]
  "",     // CEh [grey+]
  "",     // CFh [end]
  "",     // D0h [downallow]
  "",     // D1h [pgdn]
  "",     // D2h [ins]
  "",     // D3h [del]
  "",     // D4h [sysreq]
  "",     // D5h [kbd]
  "",     // D6h [left]
  "",     // D7h [f11]
  "",     // D8h [f12]
};

/**
 * this function initializes command buffer.
 */
int init_buffer_command();

/**
 * this function pushes command line strings for command buffer.
 */
int push_buffer_command(int keycode, char c);

/**
 * this function pops command line string from command buffer.
 */
int pop_buffer_command();

/**
 * this function shows from command buffer to console.
 */
int echo_command();

#endif
