#ifndef DRIVER_ATA_H
#define DRIVER_ATA_H

/**
 * @file driver_ata.h
 * @brief This header is driver of ATA device controller.
 * @author Toshimune Tanaka <t-toshi@j.email.ne.jp>
 * @date Tue, 3 Jan, 2012
 */

#include <sys/types.h>

enum {
  DEVICE_TYPE_UNKNOWN = -1,
  DEVICE_TYPE_ATA = 0,
  DEVICE_TYPE_ATAPI,
};

#define SECTOR_SIZE 256
#define BLOCK_SIZE (SECTOR_SIZE * 2)

typedef union blockdata {
  u_int16_t sector[SECTOR_SIZE];
  char data[BLOCK_SIZE];
} block_data;

/**
 * Initialize ATA devices.\n
 * Call init_common() function.
 */
int init_ata_disk_driver(void);

/**
 * Confirm ATA devices type and identify ATA devices.
 */
int init_common(int device);

/**
 * Execute software reset.
 */
void reset(int device);

/**
 * Get device type ATA or ATAPI.
 */
int get_devicetype(u_int8_t high, u_int8_t low);

/**
 * Identify ATA device.
 */
int identify_device(int device, u_int16_t *buf);

/**
 * Call wait_bitzero(), set_device_number() functions.\n
 * Also wait for DRQ and BSY registers are 0bit.
 */
int device_selection_protocol(int device);

/**
 * This function wait until BSY register to 0bit.
 */
void set_device_number(int device);

/**
 * This function wait until it is 0bit BSY and DRQ registers.
 */
int wait_bitzero(u_int16_t port);

/**
 * Set device number selected device.
 */
int wait_BSYzero(u_int16_t port);

/**
 * This function waits until it is ready for ATA device.
 */
int wait_device_ready(int device);

/**
 * Confirm input data 1 or 0.
 */
inline int is_error(u_int8_t data);

/**
 * Confirm DRQ register 1 or 0.
 */
inline int is_drq_active(u_int8_t data);

/**
 * Confirm ATA device fault.
 */
int is_device_fault(void);

/**
 * Access to read ATA device using LBA.
 */
inline int read_sector(int device,
					   u_int16_t *buf,
					   u_int32_t sector,
					   u_int8_t count);

/**
 * Open 2 ATA devices.
 */
int open_ata(void);

/**
 * Close 2 ATA devices.
 */
int close_ata(void);

#endif
