/**
 * read super block informations.
 * create:2011.08.07
 * update:2011.08.18
 * author:Toshimune Tanaka(t-toshi@j.email.ne.jp)
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include "main.h"

int main() {
  unsigned long size = 0;
  int block_count = 0;
  struct ext2_superblock e2sb;
  struct ext2_blockgroup *block_group;
  struct dentry_list head;
  int i;
  
  size = get_file_size();
  file_system = map2memory_fileimage(size);
  read_superblock(&e2sb);
  block_count = e2sb.blocks_count / e2sb.blocks_per_group + 1;
  block_group = malloc(sizeof(*block_group) * (block_count + 1));
  memset(block_group, 0x00, sizeof(*block_group));
  read_block_group(block_group, SUPER_BLOCK_SIZE * 2);
  head.next = NULL;

  printf("blocks_count:%d\n", e2sb.blocks_count);

  for(i = 0; i < block_count; i++) {
	if(block_group[i].block_bitmap != 0 &&
	   block_group[i].inode_bitmap != 0 &&
	   block_group[i].inode_table != 0) {
	  if(get_all_directories(&head,
							 get_block_data_address(&e2sb,
													block_group + i),
							 block_group + i) < 0) {
		exit(-1);
	  }
	  /**
	   * can't detect next group address.
	   */
	  if(i == 0) {
		read_block_group(block_group + 1,
						 (e2sb.blocks_per_group * 0x400) + 0x800);
	  }
	}
  }
  
  return 0;
}

static unsigned long get_file_size() {
  struct stat st;
  stat(test_file, &st);
  return st.st_size;
}

static void *map2memory_fileimage(unsigned long size) {
  int fd;
  void *ret;

  fd = open(test_file, O_RDONLY);
  ret = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0);
  close(fd);
  
  return ret;
}

static void read_superblock(struct ext2_superblock *e2sb) {
  memcpy(e2sb,
		 file_system + SUPER_BLOCK_SIZE,
		 sizeof(struct ext2_superblock));
}

static void read_block_group(struct ext2_blockgroup *e2bg,
							 unsigned long offset) {
  memcpy(e2bg,
		 file_system + offset,
		 sizeof(struct ext2_blockgroup));
}

static unsigned long get_block_data_address(struct ext2_superblock *sb,
											struct ext2_blockgroup *bg) {
  return (unsigned long) sb->inodes_per_group
	* sizeof(struct ext2_inode)
	+ blockid2address(sb, bg->inode_table);
}

static u_int32_t blockid2address(struct ext2_superblock *sb,
								 u_int32_t id) {
  return get_block_size(*sb) * id;
}

static int get_all_directories(struct dentry_list *head,
							   unsigned long address,
							   struct ext2_blockgroup *block_group) {
  int i;
  u_int16_t rec_len;
  unsigned long offset = 0;
  u_int16_t count = block_group->used_dirs_count + 2;

  for(i = 0; i < count; i++) {
	struct ext2_dentry *dentry;
	struct dentry_list *p;
	
	rec_len = read_dentry_rec_len(address, offset);
	dentry = malloc(rec_len + 1);

	read_dentry(dentry, address, offset, rec_len);

	p = malloc(sizeof(*p));

	p->dentry = dentry;
	p->next = head->next;
	head->next = p;

	printf("%s\n", dentry->name);
	offset += rec_len;
  }
  
  return 0;
}

/**
 * 後日確認
 */
static u_int16_t read_dentry_rec_len(unsigned long address,
									 unsigned long offset) {
  u_int16_t len = 0;
  memcpy(&len, file_system + address + offset + 4, sizeof(len));
  return len;
}

static void read_dentry(struct ext2_dentry *dentry,
						unsigned address,
						unsigned long offset,
						u_int16_t rec_len) {
  memcpy(dentry, file_system + address + offset, rec_len);
}
