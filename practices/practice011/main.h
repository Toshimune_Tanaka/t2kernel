#ifndef __EXT2FS_H
#define __EXT2FS_H 1

#include <sys/types.h>

#define SUPER_BLOCK_SIZE 1024

static const char const *test_file = "./fs.img";
static unsigned char *file_system;

/**
 * Super Block table.
 */
struct ext2_superblock {
  u_int32_t inodes_count;          // Total number of i-nodes
  u_int32_t blocks_count;          // Filesystem size in blocks
  u_int32_t r_blocks_count;        // Number of reserved blocks
  u_int32_t free_blocks_count;     // Free blocks counter
  u_int32_t free_inodes_count;     // Free i-nodes counter
  u_int32_t first_data_block;      // Number of first useful block(always 1)
  u_int32_t log_block_size;        // Block size
  u_int32_t log_frag_size;         // Fragment size
  u_int32_t blocks_per_group;      // Number of blocks per group
  u_int32_t frags_per_group;       // Number of fragments per group
  u_int32_t inodes_per_group;      // Number of i-nodes per group
  u_int32_t mtime;                 // Time of last mount operation
  u_int32_t wtime;                 // Time of last write operation
  u_int16_t mnt_count;             // Mount operations counter
  u_int16_t max_mnt_count;         // Number of mount operations before check
  u_int16_t magic;                 // Magic signature
  u_int16_t state;                 // Status flag
  u_int16_t errors;                // Behavior when detecting errors
  u_int16_t minor_rev_level;       // Minor revision level
  u_int32_t lastcheck;             // Time of last check
  u_int32_t checkinterval;         // Time between checks
  u_int32_t creator_os;            // OS where filesystem was created
  u_int32_t rev_level;             // Revision level of the filesystem
  u_int16_t def_resuid;            // Default UID for reserved blocks
  u_int16_t def_resgid;            // Default user group ID for reserved blocks
  u_int32_t first_ino;             // Number of first nonreserved inode
  u_int16_t inode_size;            // Size of on-disk inode structure
  u_int16_t block_group_nr;        // Block group number of this superblock
  u_int32_t feature_compat;        // Compatible features bitmap
  u_int32_t feature_incompat;      // Incompatible features bitmap
  u_int32_t feature_ro_compat;     // Read only compatible features bitmap
  u_int8_t uuid[16];               // 128-bit filesystem identifier
  u_int8_t volume_name[16];        // Volume name
  u_int8_t last_mounted[64];       // Pathname of last mount point
  u_int32_t algo_bitmap;           // Used for compression
  u_int8_t prealloc_blocks;        // Number of blocks to preallocate
  u_int8_t prealloc_dir_blocks;    // Number of blocks to preallocate for directories
  u_int8_t aling[2];               // Alignment to word
  u_int8_t journal_uuid[16];       // ???
  u_int32_t journal_inum;          // ???
  u_int32_t journal_dev;           // ???
  u_int32_t last_orphan;           // ???
  u_int32_t hash_seed[4];          // ???
  u_int8_t def_hash_version;       // ???
  u_int8_t paddiing[3];            // ???
  u_int32_t default_mount_options; // ???
  u_int32_t first_meta_bg;         // ???
  u_int8_t reserved[760];          // ???
};

/**
 * Block group descriptor table.
 */
struct ext2_blockgroup {
  u_int32_t block_bitmap;       // Block number of block bitmap
  u_int32_t inode_bitmap;       // Block number of i-node bitmap
  u_int32_t inode_table;        // Block number of first i-node table block
  u_int16_t blocks_count_free;  // Number of free blocks in the group
  u_int16_t inodes_count_free;  // Number of free i-nodes in the group 
  u_int16_t used_dirs_count;    // Number of directories int the group
  u_int16_t pad;                // Alignment to word
  u_int8_t reserved[12];        // Nulls to pad out 24 bytes
};

/**
 * i-node table.
 */
struct ext2_inode {
  u_int16_t mode;         // File type and access rights
  u_int16_t uid;          // Owner identifier
  u_int32_t size;         // File length in bytes
  u_int32_t atime;        // Time of last file access
  u_int32_t ctime;        // Time that inode last changed
  u_int32_t mtime;        // Time that file contents last changed
  u_int32_t dtime;        // Time of file deletion
  u_int16_t gid;          // User group identifier
  u_int32_t blocks;       // Number of data blocks of the file
  u_int32_t flags;        // File flags
  u_int32_t osd1;         // Specific operating system information
  u_int32_t block[15];    // Pointers to data blocks
  u_int32_t generation;   // File version (used when the file is accessed by a network filesystem)
  u_int32_t file_acl;     // File access control list
  u_int32_t dir_acl;      // Directory access control list
  u_int32_t faddr;        // Fragment address
  struct osd2 {           // Specific operating system information
	u_int8_t l_frag;        // ???
	u_int8_t l_fsize;       // ???
	u_int8_t reserved1[2];  // ???
	u_int16_t l_uid_high;   // ???
	u_int16_t l_gid_high;   // ???
	u_int8_t reserved2[2];  // ???
  } osd2;
};

struct dentry_list {
  struct ext2_dentry *dentry;
  struct dentry_list *next;
};

struct ext2_dentry {
  u_int32_t inode;
  u_int16_t rec_len;
  u_int8_t name_len;
  u_int8_t file_type;
  char name[0];
};

static unsigned long get_file_size();
static void *map2memory_fileimage(unsigned long size);
static void read_superblock(struct ext2_superblock *e2sb);
static void read_block_group(struct ext2_blockgroup *e2bg,
							 unsigned long offset);
static unsigned long get_block_data_address(struct ext2_superblock *sb,
											struct ext2_blockgroup *bg);
static u_int32_t blockid2address(struct ext2_superblock *sb,
								 u_int32_t id);
static int get_all_directories(struct dentry_list *head,
							   unsigned long address,
							   struct ext2_blockgroup *block_group);
static u_int16_t read_dentry_rec_len(unsigned long address,
									 unsigned long offset);
static void read_dentry(struct ext2_dentry *dentry,
						unsigned address,
						unsigned long offset,
						u_int16_t rec_len);

#define MIN_BLOCK_SIZE 1024
#define get_block_size(e2sb) (MIN_BLOCK_SIZE << (e2sb).log_block_size)

#endif
