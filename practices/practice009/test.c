#include <CUnit/CUnit.h>
#include <CUnit/Console.h>
#include "main.h"

void test_add(void);
void test_sub(void);

int main() {
  CU_pSuite suite;
  CU_initialize_registry();
  suite = CU_add_suite("basic functions", NULL, NULL);
  CU_add_test(suite, "test_add", test_add);
  CU_add_test(suite, "test_sub", test_sub);
  CU_console_run_tests();
  CU_cleanup_registry();

  return 0;
}

void test_add(void) {
  CU_ASSERT(add(1, 1) == 2);
}

void test_sub(void) {
  CU_ASSERT(sub(1, 1) == 0);
}
