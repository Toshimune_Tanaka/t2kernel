#include <stdio.h>
#include "main.h"

#ifdef REL
int main() {
  int ret;

  ret = 0;
  ret =  add(1, 2);
  printf("add:%d\n", ret);

  ret = 0;
  ret =  sub(2, 1);
  printf("sub:%d\n", ret);

  return 0;
}
#endif

int add(int a, int b) {
  return a + b;
}

int sub(int a, int b) {
  return a - b;
}
