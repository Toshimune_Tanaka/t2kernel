#include <stdio.h>
#include "main.h"

#ifdef REL
int main() {
  int ret;

  ret = 0;
  ret =  add(1, 2);
  printf("add:1+1=%d\n", ret);

  ret = 0;
  ret =  sub(2, 1);
  printf("sub:2-1=%d\n", ret);

  ret = 0;
  ret =  mul(2, 1);
  printf("mul:2*1=%d\n", ret);

  ret = 0;
  ret =  div(2, 1);
  printf("div:2/1=%d\n", ret);
  
  return 0;
}
#endif

int add(int a, int b) {
  return a + b;
}

int sub(int a, int b) {
  return a - b;
}

int mul(int a, int b) {
  if(a == 0 || b == 0) {
	return 0;
  } else {
	return a * b;
  }
}

int div(int a, int b) {
  if(a == 0) {
	return -1;
  } else if(b == 0) {
	return a;
  } else {
	return a / b;
  }
}
