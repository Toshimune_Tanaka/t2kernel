#include <CUnit/CUnit.h>
#include <CUnit/Console.h>
#include "main.h"

void test_add(void);
void test_sub(void);
void test_mul(void);
void test_div(void);

int main() {
  CU_pSuite suite;
  CU_initialize_registry();
  suite = CU_add_suite("basic functions", NULL, NULL);
  CU_add_test(suite, "test_add", test_add);
  CU_add_test(suite, "test_sub", test_sub);
  CU_add_test(suite, "test_mul", test_mul);
  CU_add_test(suite, "test_div", test_div);
  CU_console_run_tests();
  CU_cleanup_registry();

  return 0;
}

void test_add(void) {
  CU_ASSERT(add(1, 1) == 2);
  CU_ASSERT(add(1, 2) == 3);
  CU_ASSERT(add(1, 3) == 4);
  CU_ASSERT(add(1, 4) == 5);
  CU_ASSERT(add(1, 5) == 6);

  CU_ASSERT(add(1, 1) == 2);
  CU_ASSERT(add(2, 1) == 3);
  CU_ASSERT(add(3, 1) == 4);
  CU_ASSERT(add(4, 1) == 5);
  CU_ASSERT(add(5, 1) == 6);

  CU_ASSERT(add(9999, 9) == 10008);
  CU_ASSERT(add(9999, 99) == 10098);
  CU_ASSERT(add(9999, 999) == 10998);
  CU_ASSERT(add(9999, 9999) == 19998);
  CU_ASSERT(add(9999, 99999) == 109998);
  CU_ASSERT(add(9999, 999999) == 1009998);
  CU_ASSERT(add(9999, 9999999) == 10009998);
  CU_ASSERT(add(9999, 99999999) == 100009998);
  CU_ASSERT(add(9999, 999999999) == 1000009998);
  CU_ASSERT(add(16384, 16384) == 32768);
}

void test_sub(void) {
  CU_ASSERT(sub(1, 1) == 0);
}

void test_mul(void) {
  CU_ASSERT(mul(1, 1) == 1);
}

void test_div(void) {
  CU_ASSERT(div(1, 1) == 1);
}
