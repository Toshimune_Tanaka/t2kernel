#ifndef __LINKEDLIST_H
#define __LINKEDLIST_H 1

typedef struct LIST {
  int data;
  struct LIST *next;
} listEX;

int addList(listEX* plist, int data);
int deleteList(listEX* plist, int data);
void showList(listEX* plist);

#endif
