/**
 * Linked list sample.
 * create:2011.12.31
 * update:2011.12.31
 * author:Toshimune Tanaka(t-toshi@j.email.ne.jp)
 */

#include <stdio.h>
#include <malloc.h>
#include "main.h"

int main(void) {
  char command;
  int i;
  listEX list;
  list.next = NULL;
    
  do {
	printf("Input command\n");
	printf("    add:a\n");
	printf(" delete:d\n");
	printf("display:c\n");
	printf("> ");
	scanf(" %c", &command);
	
	switch(command) {
	case 'a':
	  printf("Input value:");
	  scanf("%d", &i);
	  addList(&list, i);
	  break;
	case 'd':
	  printf("Input value:");
	  scanf("%d", &i);
	  deleteList(&list, i);
	  break;
	case 'c':
	  showList(&list);
	  break;
	case 'q':
	  break;
	default:
	  printf("\nInvalid option.\n");
	  break;
	}
  } while(command != 'q');
  
  return 0;
}

int addList(listEX* plist, int data) {
  listEX *newcell;
  
  newcell = (listEX *)malloc(sizeof(listEX));
  if(newcell == NULL) {
	printf("Amount of memory is not enough.\n");
	return 1;
  }

  newcell->data = data;
  newcell->next = plist->next;
  plist->next = newcell;

  return 0;
}

int deleteList(listEX* plist, int data) {
  listEX *prev, *p;

  p = plist->next;
  while(p != NULL && p->data != data) {
	prev = p;
	p = p->next;
  }

  if(p == NULL) {
	printf("It doesn't exist.\n");
  } else {
	prev->next = p->next;
	free(p);
  }
    
  return 0;
}

void showList(listEX* plist) {
  listEX *p;

  for(p = plist->next; p != NULL; p = p->next) {
	printf("%d\n", p->data);
  }
}
