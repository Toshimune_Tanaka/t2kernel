		.file "loopprint.s"
		.data
msg:	.ascii "hello\n"
msize:	.equ len, msize - msg
		
		.global main
main:
		movb $0, %ah
loop:
		push %eax
		call print
		pop %eax
		
		incb %ah
		cmpb $10, %ah
		jb loop
		
		ret

print:
		movl $4, %eax
		movl $1, %ebx
		movl $msg, %ecx
		movl $len, %edx
		int $0x80
		ret
		