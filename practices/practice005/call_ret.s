		.file "call_ret.s"
		.data
		.text
		.global main
main:
		movb $1, %al
		call test_func
		call test_func
		ret

test_func:
		incb %al
		ret
		