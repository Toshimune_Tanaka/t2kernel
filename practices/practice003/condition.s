		.file "condition.s"
		.data
str_yes:	.ascii "yes\n"
str_no:	.ascii "no\n"
length_yes:	.equ len_yes, length_yes - str_yes
length_no:	.equ len_no, length_no - str_no
		
		.global main
main:
		movb $5, %al
		cmpb $5, %al
		je printA
		
		movb $4, %al
		cmpb $5, %al
		je printB

		ret

printA:
		movl $4, %eax
		movl $1, %ebx
		movl $str_yes, %ecx
		movl $len_yes, %edx
		int $0x80
		ret

printB:
		movl $4, %eax
		movl $1, %ebx
		movl $str_no, %ecx
		movl $len_no, %edx
		int $0x80
		ret