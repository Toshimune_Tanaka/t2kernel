/**
 * @file descriptor.c
 * @brief Some descriptor source file.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @date 2011-02-20
 */
#include "descriptor.h"

/**
 * @brief initialize GTD and IDT.
 * @param -
 * @return -
 */
void init_gdtidt(void) {
  struct SEGMENT_DESCRIPTOR *gdt = (struct SEGMENT_DESCRIPTOR *) ADDRESS_GDT;
  struct GATE_DESCRIPTOR *idt = (struct GATE_DESCRIPTOR *) ADDRESS_IDT;
  int i;

    // initialize GDT
  for(i = 0; i < 8192; i++) {
	set_segmentdescriptor(gdt + i, 0, 0, 0);
  }
  // set GDT
  set_segmentdescriptor(gdt + 1,
						0xffffffff, // limit
						0x00000000, // base
						//						0x409a);    // access_right, limit_high
						0xdf9a);
  set_segmentdescriptor(gdt + 2,
						0xffffffff, // limit
						0x00000000, // base
						//						0x4092);    // access_right, limit_high
						0xdf92);
  
  load_gdtr(0x0000ffff, ADDRESS_GDT);

  // initialize IDT
  for(i = 0; i < 256; i++) {
	set_gatedescriptor(idt + i, 0, 0, 0);
  }
  load_idtr(0x000007ff, ADDRESS_IDT);
  
  // set IDT
  // + 0x20 = 32 * 8byte
  set_gatedescriptor(idt + 0x20,
					 (int)&INTERRUPT_TIMER,  // offset
					 8,                      // selector
					 0x008e);                // offset_high
  // + 0x21 = 33 * 8byte
  set_gatedescriptor(idt + 0x21,
					 (int)&INTERRUPT_KEYBOARD, // offset
					 8,                        // selector
					 0x008e);                  // offset_high
  // + 0x2c = 44 * 8byte
  /**
  set_gatedescriptor(idt + 0x2c,
					 (int)&INTERRUPT_MOUSE,    // offset
					 8,                        // selector
					 0x008e);                  // offset_high
  */
  return;
}

/**
 * @brief set segumentdescriptor.
 * @param SEGMENT_DESCRIPTOR
 * @param limit
 * @param base
 * @param ar
 * @return -
 */
void set_segmentdescriptor(struct SEGMENT_DESCRIPTOR *sd,
						   unsigned int limit,
						   int base,
						   int ar) {
  if(limit > 0xfffff) {
	ar |= 0x8000; /* G_bit = 1 */
	limit /= 0x1000;
  }

  sd->limit_low = limit & 0xffff;
  sd->base_low = base & 0xffff;
  sd->base_mid = (base >> 16) & 0xff;
  sd->access_right = ar & 0xff;
  sd->limit_high = ((limit >> 16) & 0x0f) | ((ar >> 8) & 0xf0);
  sd->base_high = (base >> 24) & 0xff;
    
  return;
}

/**
 * @brief set gatedescriptor
 * @param GATE_DESCRIPTOR
 * @param offset
 * @param selector
 * @param ar
 * @return -
 */
void set_gatedescriptor(struct GATE_DESCRIPTOR *gd,
						int offset,
						int selector,
						int ar) {
  gd->offset_low = offset & 0xffff;
  gd->selector = selector;
  gd->dw_count = (ar >> 8) & 0xff;
  gd->access_right = ar & 0xff;
  gd->offset_high = (offset >> 16) & 0xffff;
  
  return;
}
