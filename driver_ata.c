/**
 * @file driver_ata.c
 * @brief This source is driver of ATA device controller.
 * @author Toshimune Tanaka<t-toshi@j.email.ne.jp>
 * @date Tue, 20 Nov, 2012
 */

#include "driver_ata.h"
#include "i386.h"
#include "video.h"
#include "timer.h"
#include <sys/io.h>

/**
 * @brief Initialize ATA devices.
 * @date Fri, 24 Dec, 2011
 * @param void
 * @return int 0:Success other:Error code
 */
int init_ata_disk_driver(void) {
  int ret = 0;

  ret = init_common(0);
  
  if(ret) {
	ret = init_common(1);
	if(!ret) {
	  printf("Unable to setup slave\n");
	  ret = 1;
	}
  }
  
  return ret;
}

/**
 * @brief Confirm ATA devices type and identify ATA devices.
 * @date Fri, 24 Dec, 2011
 * @param int device
 * @return int 0:Success other:Error code
 */
int init_common(int device) {
  u_int8_t high, low;
  u_int16_t buf[SECTOR_SIZE];
  int dev = 0;
  int ret = 1;

  memset(buf, 0x00, sizeof(buf));
  high = 0;
  low = 0;

  reset(device);

  low = in8(0x01f4);
  high = in8(0x01f5);

  /**
   * comfirm device type.
   */
  device = get_devicetype(high, low);
  if(dev == DEVICE_TYPE_ATA) {
	printf("ATA device is supported.\n");
	ret = 0;
  } else if(dev == DEVICE_TYPE_ATAPI) {
	printf("ATAPI device is not supported.\n");
	ret = 1;
  } else if(dev == DEVICE_TYPE_UNKNOWN) {
	printf("Unknown device connecting.\n");
	ret = 1;
  } else {
	printf("Unknown device connecting.\n");
	ret = 1;
  }

  ret = identify_device(device, buf);
  if(0 != ret) {
	printf("Identify device failed.\n");
	return 1;
  }
   
  return ret;
}

/**
 * @brief Execute software reset.
 * @date Sat, 24 Dec, 2011
 * @param int device
 * @return int 0:Success other:Error code
 */
void reset(int device) {
  set_device_number(device);
  out8(0x01f7, 0x08);
  wait_usec(5);
}

/**
 * @brief Get device type ATA or ATAPI.
 * @date Fri, 24 Dec, 2011
 * @param u_int8_t high
 * @param u_int8_t low
 * @return int 0:Success other:Error code
 */
int get_devicetype(u_int8_t high, u_int8_t low) {
  if(high == 0x00 && low == 0x00) {
	return DEVICE_TYPE_ATA;
  } else if(high == 0xeb && low == 0x14) {
	return DEVICE_TYPE_ATAPI;
  } else {
	return DEVICE_TYPE_UNKNOWN;
  }
}

/**
 * @brief Identify ATA device.
 * @date Fri, 24 Dec, 2011
 * @param int device
 * @param u_int16_t *buf
 * @return int 0:Success other:Error code
 */
int identify_device(int device, u_int16_t *buf) {
  int ret = 0;
  u_int8_t data;
  int i, addr;

  device_selection_protocol(device);
  data = in8(0x01f7);
  ret = (data >> 6) & 0x01;

  if(ret) {
	out8(0x01f7, 0xec);
	wait_usec(5);
	
	if(wait_BSYzero(0x01f7)) {
	  printf("Wait failed.\n");
	  return 1;
	}

	data = in8(0x03f6);
  read_statusregister:
	data = in8(0x01f7);
	
	if(!is_error(data)) {
	  printf("Error occured:0x%x\n", data);
	  return 1;
	}

	if(is_drq_active(data)) {
	  goto read_statusregister;
	}

	if(!is_device_fault()) {
	  printf("Error occured\n");
	  return 1;
	}

	for(i = 0; i < SECTOR_SIZE; i++) {
	  buf[i] = inw(0x01f0);
	}

	ret = 0;
  }
  
  return ret;
}

/**
 * @brief 
 * @date Fri, 24 Dec, 2011
 * @param int device
 * @return int 0:Success other:Error code
 */
int device_selection_protocol(int device) {
  int ret = 1;

  ret = wait_bitzero(0x03f6);
  
  if(0 == ret) {
	set_device_number(device);
	wait_usec(2);
	ret = wait_bitzero(0x03f6);
  }
  
  return ret;
}

/**
 * @brief This function wait until it is 0bit BSY and DRQ registers.
 * @date Fri, 24 Dec, 2011
 * @param u_int16_t port
 * @return int 0:Success other:Error code
 */
int wait_bitzero(u_int16_t port) {
  u_int8_t data = 0xff;
  u_int8_t r_bsy, r_drq;
  int i = 0;

  do {
	data = in8(port);
	r_bsy = (data >> 7) & 0x01;
	r_drq = (data >> 3) & 0x01;
	i++;
  } while(!(!r_bsy && !r_drq) && i < 1024);

  return (r_bsy == 0 && r_drq == 0) ? 0 : 1;
}

/**
 * @brief This function wait until it is 0bit BSY register.
 * @date Fri, 24 Dec, 2011
 * @param u_int16_t port
 * @return int 0:Success other:Error code
 */
int wait_BSYzero(u_int16_t port) {
  int ret = 1;
  u_int8_t data = 0xff;
  u_int8_t r_bsy;
  int i = 0;

  for(i = 0; i < 1024; i++) {
	data = in8(port);
	r_bsy = (data >> 7) & 0x01;
	if(r_bsy == 0) {
	  ret = 0;
	  break;
	}
  }

  return ret;
}

/**
 * @brief Set device number selected device.
 * @date Fri, 24 Dec, 2011
 * @param int device
 * @return void
 */
void set_device_number(int device) {
  u_int8_t data = 0;
  
  data = in8((u_int16_t) 0x01f6);
  
  if(!device) {
	data &= 0x0f;
  } else {
	data |= 0x10;
  }

  out8((u_int16_t) 0x01f6, data);

  wait_usec(5);
}

/**
 * @brief This function Waits until it is ready for ATA device.
 * @date Sun, 25 Dec, 2011
 * @param int device
 * @return 0:Success other:Error code
 */
int wait_device_ready(int device) {
  int i;
  int ret = 1;

  for(i = 0; i < 5; i++) {
	ret = device_selection_protocol(device);
	if(!ret) {
	  break;
	}
	wait_usec(5);
  }

  return ret;
}

/**
 * @brief Confirm input data 1 or 0.
 * @date Fri, 24 Dec, 2011
 * @param u_int8_t data
 * @return int 0:Success other:Error code
 */
inline int is_error(u_int8_t data) {
  return (data & 0x01) == 0 ? 1 : 0;
}

/**
 * @brief Confirm DRQ register 1 or 0.
 * @date Fri, 24 Dec, 2011
 * @param u_int8_t data
 * @return int 0:Success other:Error code
 */
inline int is_drq_active(u_int8_t data) {
  return (((data >> 3) & 0x01) == 1) ? 0 : 1;
}

/**
 * @brief Confirm ATA device fault.
 * @date Fri, 24 Dec, 2011
 * @return int 0:Success other:Error code
 */
int is_device_fault(void) {
  u_int8_t data;

  data = in8(0x01f7);

  return (data >> 5 & 0x01) == 1 ? 0 : 1;
}

/**
 * @brief Access to read ATA device using LBA.
 * @date Sun, 15 Jan, 2012
 * @param int device
 * @param u_int32_t sector
 * @param u_int8_t count
 * @return int 0:Success other:Error code
 */
inline int read_sector(int device,
					   u_int16_t *buf,
					   u_int32_t sector,
					   u_int8_t count) {
  int ret = 1;
  u_int8_t data;
  int i;
  int loop = 0;
  
  memset(buf, 0x00, sizeof(buf));
  
  ret = wait_device_ready(device);
  
  if(0 != ret) {
	printf("Failed to read sector.\n");
	return 1;
  }

  out8(0x03f6, 0x02);
  out8(0x01f1, 0x00);

  out8(0x01f2, count);                          // Read count.
  out8(0x01f3, sector & 0xff);                  // Sector number register.
  out8(0x01f4, (sector >> 8) & 0xff);           // Cylinder Low register.
  out8(0x01f5, (sector >> 16) & 0xff);          // Cylinder High register.
  out8(0x01f6, ((sector >> 24) & 0x1f) | 0x40); // Head register.

  //printf("device number:0x%x \n", device);
  //printf("read count:0x%x \n", count);
  //printf("sector number:0x%x \n", sector & 0xff);
  //printf("cylinder high:0x%x \n", (sector >> 8) & 0xff);
  //printf("cylinder low:0x%x \n", (sector >> 16) & 0xff);
  //printf("cylinder head:0x%x \n", (((sector >> 24) & 0x0f) | 0x40));

  out8(0x01f7, 0x20); // read data.
  wait_usec(4);

  in8(0x03f6);

 read_statusregister_again:
  data = in8(0x01f7);
  if(!is_error(data)) {
	printf("Error occured:0x%x\n", data);
	return 1;
  }
  
  if(is_drq_active(data)) {
	if(loop > 5) {
	  printf("DRQ didn't be active.\n");
	  return 1;
	}
	loop++;
	goto read_statusregister_again;
  }
  
  u_int16_t value = 0x00;
  for(i = 0; i < SECTOR_SIZE * count; i++) {
  	value = inw(0x01f0);
  	//buf[i] = ((value & 0xff) << 8) | ((value >> 8) & 0xff);
	buf[i] = value;
  }

  /*
  for(i = 0; i < SECTOR_SIZE * count; i++) {
  	printf("%x ", buf[i]);
  }
  printf("\n");
  printf("Done.\n");
  */
  
  in8(0x03f6);
  in8(0x01f7);
  
  return 0;
}

/**
 * @brief Open 2 ATA devices.
 * @date Tue, 3 Jan, 2012
 * @return int 0:Success other:Error code
 */
int open_ata(void) {
  int ret = 0;

  ret = init_ata_disk_driver();
  if(ret != 0) {
	return 1;
  }

  return ret;
}

/**
 * @brief Close 2 ATA devices.
 * @date Tue, 3 Jan, 2012
 * @return int 0:Success other:Error code
 */
int close_ata(void) {
  int ret = 0;
  
  return ret;
}
