#ifndef DESCRIPTOR_H
#define DESCRIPTOR_H

/**
 * @file descriptor.h
 * @brief Some descriptor header file.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @date 2011-02-20
 */

#define ADDRESS_GDT 0x00270000
#define ADDRESS_IDT 0x0026f800
#define START_MEMORY 0x00400000
#define END_MEMORY 0xbfffffff


/**
 * segment descriptor.
 */
struct SEGMENT_DESCRIPTOR {
  short limit_low;
  short base_low;
  char base_mid;
  char access_right;
  char limit_high;
  char base_high;
} __attribute__((__packed__));

/**
 * gate descriptor.
 */
struct GATE_DESCRIPTOR {
  short offset_low;
  short selector; 
  char dw_count;
  char access_right;
  short offset_high;
} __attribute__((__packed__));

/**
 * initialize GDT and IDT.
 */
void init_gdtidt(void);
/**
 * set segmentdescriptor.
 */
void set_segmentdescriptor(struct SEGMENT_DESCRIPTOR *sd,
						   unsigned int limit,
						   int base,
						   int ar);
/**
 * set gatedescriptor.
 */
void set_gatedescriptor(struct GATE_DESCRIPTOR *gd,
						int offset,
						int selector,
						int ar);
extern INTERRUPT_KEYBOARD;
//extern INTERRUPT_MOUSE;
extern INTERRUPT_TIMER;
extern void load_gdtr(int limit, int address);
extern void load_idtr(int limit, int address);
extern void load_tr(int tr);
extern void taskswitch3(void);
extern void taskswitch4(void);
extern int load_eflags(void);
extern void store_eflags(int eflags);
extern int load_cr0(void);
extern void store_cr0(int cr0);

#endif
