#ifndef VIDEO_H
#define VIDEO_H

/**
 * @file video.h
 * @brief Video control system call header file.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @date 2011-02-20
 */

//#define BLANK ((0x0F) << 8) | 0x20
#define BLANK ((0x0A) << 8) | 0x20

/**
 * clear screen.
 */
void clear_screen(void);

/**
 * initialize video control.
 */
void init_video(void);

/**
 * set cursor position.
 */
void set_cursor(void);

/**
 * scroll up cursor.
 */
void scroll_up_cursor(void);

/**
 * out put charactor to screen.
 */
void outputchar(char);

/**
 * out put strings to screen.
 */
void putString(const char *);

/**
 * ini to string.
 */
char * uint_to_str(char *buf, unsigned src, int base);

/**
 * printf function.
 */
void printf(char * fmt, ...);

#endif
