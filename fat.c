/**
 * @file fat.c
 * @brief This source is driver of ATA device controller.
 * @author Toshimune Tanaka<t-toshi@j.email.ne.jp>
 * @date Tue, 20 Nov, 2012
 */

#include <string.h>
#include "fat.h"
#include "video.h"

int read_masterbootrecord(u_int16_t pbp[SECTOR_SIZE]) {
  u_int16_t sector[SECTOR_SIZE];
  int ret = -1;

  if(read_sector(0, sector, 0x6, 1) != 0) {
	ret = -1;
	printf("Failer to read sector.\n");
  } else {
	ret = 0;
  }

  memcpy(pbp, sector, 512);

  return ret;
}

int read_sector_fromFAT(u_int16_t FatNumber,
						u_int16_t sector[SECTOR_SIZE],
						u_int16_t count) {
  int ret = -1;

  if(read_sector(0, sector, FatNumber, count) != 0) {
	ret = -1;
	printf("Failer to read sector.\n");
  } else {
	ret = 0;
  }

  return ret;
}

