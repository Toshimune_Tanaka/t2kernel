/**
 * @file task_shell.c
 * @brief shell function task source file.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @date Sun 03-07-2011
 */
#include "i386.h"
#include "video.h"
#include "buffer.h"
#include "fifo.h"
#include "kernel.h"
#include "descriptor.h"
#include "task_shell.h"

/**
 * @brief shell function task entory point.
 * @param -
 * @return -
 */
void shell_main(void) {
  // for timer functions.
  struct FIFOBUF fifo;
  struct TIMER *timer;
  char fifobuf[128];

  int i;

  // for keycode functions.
  char keybuf[32];
  int keycode = 0;
  char* str = 0;
  int key_shift = 0;

  fifo_init(&fifo, 128, fifobuf);
  timer = timer_alloc();
  timer_init(timer, &fifo, 1);
  timer_settime(timer, 1);

  fifo_init(&keyfifo, 32, keybuf);
  init_buffer_command();
  
  printf(">");
  while(1) {
	cli();
	if(fifo_status(&fifo) == 0) {
	  stihlt();
	  //printf("%d", fifo_status(&fifo));
	} else {
	  i = fifo_get(&fifo);
	  sti();
	  if(i == 1) {
		//printf("task B test message.\n");
		//printf("%d", fifo_status(&fifo));
		taskswitch(3 * 8);
		timer_settime(timer, 1);
		cli();
	  }
	}

	if(fifo_status(&keyfifo) == 0) {
	  stihlt();
	} else {
	  keycode = fifo_get(&keyfifo);
	  sti();
	  str = 0x00;
	  if(key_shift == 0) {
		str = KeyTable_NoShift[keycode];
	  } else {
		str = KeyTable_Shift[keycode];
	  }

	  if(0x00 <= keycode && 0x7F >= keycode) {
		// section of output after keycode to character.
		push_buffer_command(keycode, *str);
		printf("%s", str);
		if(0x1C == keycode) {
		  echo_command();
		  printf(">");
		  init_buffer_command();
		}
	  } else if(0x80 <= keycode && 0xFF >= keycode) {
		// section of output after keycode to character.
		;
	  } else {
		// case of can't understand keycode.
		printf("??:%x:%s\r\n", keycode, str);
	  }
	  
	  // left shift ON
	  if(keycode == 0x2a) {
		key_shift |= 1;
	  }
	  // right shift ON
	  if(keycode == 0x36) {
		key_shift |= 2;
	  }
	  // left shift OFF
	  if(keycode == 0x80 + 0x2a) {
		key_shift &= ~1;
	  }
	  // right shift OFF
	  if(keycode == 0x80 + 0x36) {
		key_shift &= ~2;
	  }
	}
  }
}
