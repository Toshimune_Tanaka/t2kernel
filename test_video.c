/**
 * @file test_video.c
 * @brief test source file of video functions.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @date 2011-02-20
 */
#include <CUnit/CUnit.h>
#include <CUnit/Console.h>
#include "video.h"

void test_clear_screen(void);
void test_init_video(void);
void test_scroll_up_cursor(void);
void test_outputchar(void);
void test_putString(void);
void test_uint_to_str(void);
void test_printf(void);

int main() {
  CU_pSuite suite;
  CU_initialize_registry();
  
  suite = CU_add_suite("basic functions", NULL, NULL);

  CU_add_test(suite, "test_clear_screen", test_clear_screen);
  CU_add_test(suite, "test_init_video", test_init_video);
  CU_add_test(suite, "test_scroll_up_cursor", test_scroll_up_cursor);
  CU_add_test(suite, "test_outputchar", test_outputchar);
  CU_add_test(suite, "test_putString", test_putString);
  CU_add_test(suite, "test_uint_to_str", test_uint_to_str);
  CU_add_test(suite, "test_printf", test_printf);
  
  CU_console_run_tests();
  CU_cleanup_registry();

  return 0;
}

void test_clear_screen(void) {
  //CU_ASSERT(clear_screen() == -1);
}

void test_init_video(void) {
  //CU_ASSERT(init_video() == -1);
}

void test_scroll_up_cursor(void) {
  //CU_ASSERT(scroll_up_cursor() == -1);
}

void test_outputchar(void) {
  //CU_ASSERT(outputchar() == -1);
}

void test_putString(void) {
  //CU_ASSERT(putString() == -1);
}

void test_uint_to_str(void) {
  //CU_ASSERT(uint_to_str() == -1);
}

void test_printf(void) {
  //CU_ASSERT(printf() == -1);
}
