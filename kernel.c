/**
 * \mainpage T2-Kernel manual Index Page.
 * \section intro Introduction
 * This is the introduction.
 * \section outline outlines
 * This is the outlines.
 * \section install Installation
 * \subsection step1 Step 1: ********
 * \subsection step2 Step 2: ********
 * \subsection step3 Step 3: ********
 * \subsection step4 Step 4: ********
 * \subsection step5 Step 5: ********
 * \subsection step6 Step 6: ********
 * \subsection step7 Step 7: ********
 * etc...
 */

/**
 * @file kernel.c
 * @brief T2-Kernel(Original OS) source file.
 * @author Toshimune Tanaka <t-toshi@j.email.ne.jp>
 * @date Sun, 23 Dec, 2012
 */
#include <stdlib.h>

#include "i386.h"
#include "video.h"
#include "buffer.h"
#include "fifo.h"
#include "kernel.h"
#include "descriptor.h"
#include "task_shell.h"
#include "driver_ata.h"
#include "ext2.h"
#include "fat.h"

/**
 * @brief Entory original OS kernel.
 * @param -
 * @return -
 */
void start_kernel() {
  init_gdtidt();
  init_pic();
  sti();
  init_pit();
  /**
   * enable PIT and PIC1 and keyboard [11111000]
   */
  out8(0x21, 0xf8);
  /**
   * enable mouse [11101111]
   */
  
  /**
   * out8(0xa1, 0xef);
   */
  init_video();

  /**
   * Open ATA devices.
   */
  if(open_ata() == 0) {
	printf("ATA devices has been successfully opened.\n");
  } else {
	printf("Failed to open the ATA devices.\n");
  }

  struct TaskStateSegment_32 taskstatesegment_main, taskstatesegment_shell;
  struct SEGMENT_DESCRIPTOR *gdt =
	(struct SEGMENT_DESCRIPTOR *) ADDRESS_GDT;

  taskstatesegment_main.ldtr = 0;
  taskstatesegment_main.iomap = 0x40000000;
  taskstatesegment_shell.ldtr = 0;
  taskstatesegment_shell.iomap = 0x40000000;
  set_segmentdescriptor(gdt + 3,
						103,
						(int)&taskstatesegment_main,
						0x0089);
  set_segmentdescriptor(gdt + 4,
						103,
						(int)&taskstatesegment_shell,
						0x0089);
  load_tr(3 * 8);
  taskstatesegment_shell.eip = (int)&shell_main;
  taskstatesegment_shell.eflags = 0x00000246;  // IF = 1;
  taskstatesegment_shell.cr3 = 0;
  taskstatesegment_shell.eax = 0;
  taskstatesegment_shell.ecx = 0;
  taskstatesegment_shell.edx = 0;
  taskstatesegment_shell.ebx = 0;
  taskstatesegment_shell.esp = 0x1000000;
  taskstatesegment_shell.ebp = 0;
  taskstatesegment_shell.esi = 0;
  taskstatesegment_shell.edi = 0;
  taskstatesegment_shell.cs = 1 * 8;
  taskstatesegment_shell.es = 2 * 8;
  taskstatesegment_shell.ss = 2 * 8;
  taskstatesegment_shell.ds = 2 * 8;
  taskstatesegment_shell.fs = 2 * 8;
  taskstatesegment_shell.gs = 2 * 8;

  char mcursor[256];
  char timerbuf[8];
  struct FIFOBUF timerfifo;
  struct TIMER *timer;
  
  fifo_init(&timerfifo, 8, timerbuf);
  timer = timer_alloc();
  timer_init(timer, &timerfifo, 1);
  timer_settime(timer, 1);

  /**
   * call check memory function.
   */
  printf("start checking memory.\n");
  int num_memory;
  num_memory = 0;
  num_memory = test_memory(START_MEMORY, END_MEMORY) / (1024 * 1024);
  printf("complete checking memory:%dMB\n", num_memory);
  
  /**
   * Reading test from EXT2 File system....
   */

  u_int8_t buffer[SECTOR_SIZE * 2];
  if(read_masterbootrecord(&buffer) != 0) {
	printf("Failer to read FAT MBR block.\n");
  }

  u_int16_t *BPB_BytesPerSector = &buffer[0x0B];
  u_int8_t *BPB_SectorsPerCluster = &buffer[0x0D];
  u_int16_t *BPB_ReservedSector = &buffer[0x0E];
  u_int8_t *BPB_NumberOfFATs = &buffer[0x10];
  u_int16_t *BPB_RootEntryCount = &buffer[0x11];
  u_int16_t *BPB_SectorsPerTrack = &buffer[0x18];
  u_int32_t *BPB_FatSize32 = &buffer[0x24];
  u_int32_t *BPB_RootCluster = &buffer[0x2c];
  u_int32_t *BPB_Sig = &buffer[0x01FE];
  
  u_int32_t FatSector =
	*BPB_ReservedSector + 1 * 4 / *BPB_SectorsPerTrack;
  u_int32_t FirstDataSector =
	*BPB_ReservedSector + *BPB_FatSize32 * 2;
  
  printf("  BPB_BytesPerSector[Offset:0x000B]:0x%x\n",
		 *BPB_BytesPerSector);
  printf("BPB_SectorPerCluster[Offset:0x000D]:0x%x\n",
		 *BPB_SectorsPerCluster);
  printf("  BPB_ReservedSector[Offset:0x000E]:0x%x\n",
		 *BPB_ReservedSector);
  printf("    BPB_NumberOfFATs[Offset:0x0010]:0x%x\n",
		 *BPB_NumberOfFATs);
  printf("  BPB_RootEntryCount[Offset:0x0011]:0x%x\n",
		 *BPB_RootEntryCount);
  printf(" BPB_SectorsPerTrack[Offset:0x0018]:0x%x\n",
		 *BPB_SectorsPerTrack);
  printf("       BPB_FatSize32[Offset:0x0024]:0x%x\n",
		 *BPB_FatSize32);
  printf("     BPB_RootCluster[Offset:0x002C]:0x%x\n",
		 *BPB_RootCluster);
  printf("             BPB_Sig[Offset:0x01fE]:0x%x\n",
		 *BPB_Sig);
  printf("                      FAT_FatSector:0x%x\n",
		 FatSector);
  printf("                           FAT_Size:0x%x\n",
		 *BPB_FatSize32 * 2);
  printf("                FAT_FirstDataSector:0x%x\n",
		 FirstDataSector);

  /**
   * Read FATs
   */
  unsigned char FAT[SECTOR_SIZE];
  read_sector_fromFAT(FatSector, &FAT, 1);
  int i = 0;
  unsigned int offset = 4;
  for(i = 0; i < 4; i++) {
	printf("%d: %x \n", i, *(unsigned int*)&FAT[offset * i] & 0x0FFFFFFF);
  }

  /**
   * Read Datas
   */
  u_int16_t data[SECTOR_SIZE];
  read_sector_fromFAT(FirstDataSector, &data, 1);
  for(i = 0; i < SECTOR_SIZE; i++) {
	printf("%x ", data[i]);
  }
  printf("\n");
  
  while(1) {
	cli();

	if(fifo_status(&timerfifo) != 0) {
	  sti();
	  //printf("task A test message.\n");
	  taskswitch(4 * 8);
	  timer_settime(timer, 1);
	  cli();
	}
  }

  while(1) {
	hlt();
  }
}
