#ifndef I386_H
#define I386_H

/**
 * @file i386.h
 * @brief Peer platform header file.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @date 2012-02-20
 */

/**
 * 
 */
typedef struct {
  unsigned short offset;
  unsigned int selector;
} jmppointer;

/**
 * @brief "out8" system call. mem read after this function is called.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @date 2010-02-10
 */
static __inline__ void out8(unsigned short port, unsigned char value) {
  __asm__ __volatile__ ("out %%al, %%dx" : : "a" (value), "d" (port));
}

/**
 * @brief "in8" system call. mem write after this function is called.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @date 2010-03-07
 */
static __inline__ unsigned char in8(unsigned short port) {
  register unsigned char value;
  __asm__ __volatile__ ("in %%dx, %%al" :"=a" (value) : "d" (port));
  return value;
}

/**
 * taskswitch function.
 */
static __inline__ void taskswitch(unsigned int selector) {
  jmppointer jmpptr;
  jmpptr.offset = 0;
  jmpptr.selector = selector;
  __asm__ __volatile__ ("ljmp %0 ;"
						: // not output parameter
						:"m" (jmpptr));
  return;
}

extern void sti();
extern void cli();
extern void hlt();
extern void stihlt();

#endif
