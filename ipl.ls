/*
ipl.ls
author: Toshimune Tanaka(t-toshi@j.email.ne.jp)
create: 2010.02.28
update: 2010.11.15
*/

SECTIONS {
		 . = 0x1000;
		 _kernel_begin = .;
		 .ipl 0x1000:{
		 	  ipl.o;
			  setup.o;
			  *(EXCLUDE_FILE(ipl.o setup.o)*);
		 }
		 . = ALIGN(32);
		 _kernel_end = .;
		 _kernel_end_sector = ((_kernel_end - _kernel_begin) + 511) / 512;
}
