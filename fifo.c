/**
 * @file fifo.c
 * @brief fifo function source file.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @date 2011-02-20
 */
#include "buffer.h"
#include "fifo.h"

#define FLAGS_OVERRUN 0x0001

/**
 * @brief put data to FIFO buffer.
 * @param FIFOBUF
 * @param data
 * @return int
 */
int fifo_put(struct FIFOBUF *fifo, unsigned char data) {
  if(fifo->free == 0) {
	fifo->flags |= FLAGS_OVERRUN;
	return -1;
  } else {
	//
  }
  fifo->data[fifo->p] = data;
  fifo->p++;
  if(fifo->p == fifo->size) {
	fifo->p = 0;
  }
  fifo->free--;
  
  return 0;
}

/**
 * @brief get data from FIFO buffer.
 * @param FIFOBUF
 * @return int
 */
int fifo_get(struct FIFOBUF *fifo) {
  int data;
  if(fifo->free == fifo->size) {
	return -1;
  } else {
	//
  }
  data = fifo->data[fifo->q];
  fifo->q++;
  if(fifo->q == fifo->size) {
	fifo->q = 0;
  }
  fifo->free++;
  
  return data;
}

/**
 * @brief check FIFO buffer status.
 * @param FIFOBUF
 * @return int
 */
int fifo_status(struct FIFOBUF *fifo) {
  return fifo->size - fifo->free;
}

/**
 * @brief initialize FIFO buffer.
 * @param FIFOBUF
 * @param size
 * @param data
 * @return -
 */
void fifo_init(struct FIFOBUF *fifo, int size, unsigned char *data) {
  fifo->size = size;
  fifo->data = data;
  fifo->free = size;
  fifo->flags = 0;
  fifo->p = 0; // write position
  fifo->q = 0; // read position
}
