		/*
		ipl.S
		author:Toshimune Tanaka(t-toshi@j.email.ne.jp)
		create:2010.02.28
		update:2010.11.15
		*/
		/*
		constants
		*/
		boot_begin = 0x7c00
		ipl_size = 0x200
		fd_sector_per_track = 0x12
		fd_sector_length = 512
		kernel_begin_sector = 0x01
		read_kernel_es_offset = 0x1000

		/*
		generate real mode code
		*/
		.code16
		jmp	begin
		nop
		
		/*
		BPB
		*/
name:			.ascii	"Name ... "
sector_size:	.word	0x0200
cluster_size:	.byte	0x01
fat_pos:		.word	0x0001
fat_cnt:		.byte	0x02
root_size:		.word	0x00e0
sector_cnt:		.word	0x0b40
media_type:		.byte	0xf0
fat_size:		.word	0x0009
sector_cnt_pt:	.word	0x0012
head_cnt:		.word	0x0002
bpb_pos:		.long	0x0000
sector_cnt_l:	.long	0x00000b40
drive_no:		.byte	0x00
reserved:		.byte	0x00
ext_boot_code:	.byte	0x29
volume_serial:	.long	0xffffffff
disk_name:		.ascii	"DISK ... "
fat_name:		.ascii	"FAT12 ... "
		
		/*
		boot start
		*/
begin:
		cli							/* deny interrupt */
		cld							/* setup registers */
		xorw %ax, %ax
		movw %ax, %sp
		movw %ax, %es
		movw %ax, %fs
		movb %dl, boot_drive		/* save_boot drive*/
		movw $_kernel_begin, %sp	/* setup stack pointer */
		xor %ax, %ax				/* move ipl to kernel begin */
		movw %ax, %ds				/* source */
		movw $boot_begin, %si
		movw %ax, %es				/* dest */
		movw $_kernel_begin, %di
		movw $(ipl_size / 2), %cx	/* count */
		rep movsw					/* move */
		ljmp $0x0000, $set_cs		/* setup code segment */
		
set_cs:
		movw $BOOT_MSG, %si			/* show message */
		call print
		movb %dl, boot_drive		/* reset disk drive */
		call reset_disk
		jc error
		movw $DISK_RESET_MSG, %si	/* success */
		call print
		
		pushw $0x00					/* read kernel image from disk */
		popw %es
		xorw  %di, %di
		movw $ipl_end, %di
		movb boot_drive, %dl
		movw $kernel_begin_sector, %si
		
read_kernel:
		call read_disk_sector
		jc error
		addw $fd_sector_length, %di
		jnc advance_read_kernel_sector
		movw %es, %ax
		addw $read_kernel_es_offset, %ax
		movw %ax, %es
		
advance_read_kernel_sector:
		incw %si
		cmpw $_kernel_end_sector, %si
		jb read_kernel
		movw $DISK_READ_MSG, %si		/* success */
		call print
		jmp ipl_end
		
		/*
		error
		*/
error:
		movw $ERROR_MSG, %si
		call print
		
error_end:
		jmp error_end
		
end:	
		jmp	end
		
		/*
		print string
			params:
				si = string address
		*/
print:
		xorw %bx, %bx
		movb $0x0e, %ah
print_char:
		lodsb
		orb %al, %al
		jz print_end
		int $0x10
		jmp print_char
print_end:
		ret

		/*
		reset disk system
		params:
			dl = drive no
		returns:
			al = error code. 0 if successed.
		*/
reset_disk:
		xor %al, %al
		xor %ah, %ah
		int $0x13
		ret

		/*
		read disk sector
		params:
			es:di = dest buffer address
			si = source sector no (0 - 2879)
			dl = drive no
		returns:
			al = error code. 0 if successed
		*/
read_disk_sector:
		pushw %dx						/* save drive no */
		xorw %dx, %dx
		movw %si, %ax
		movb $fd_sector_per_track, %dl	/* logical sector to track no */
		divb %dl
		movb %al, %ch					/* track no */
		shrb $0x01, %ch
		jnc head_0						/* head no */
		movb $0x01, %dh

head_0:
		movb %ah, %cl					/* sector no */
		incb %cl
		movw %di, %bx					/* dest buffer */
		popw %ax						/* drive no */
		movb %al, %dl
		movb $0x01, %al					/* sector count */
		movb $0x02, %ah					/* read command */
		int $0x13						/* execute */
		ret
		
		/*
		boot message
		*/
BOOT_MSG:		.string	"Boot start.\r\n"
DISK_RESET_MSG:	.string "Success reset disk.\r\n"
DISK_READ_MSG:	.string "Success read disk.\r\n"
ERROR_MSG:		.string "Disk error!\r\n"
boot_drive:		.byte 0x00
		
		/*
		boot signature
		*/
		. = 510
		.short 0xaa55
ipl_end: