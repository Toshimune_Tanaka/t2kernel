#include "mem.h"

/**
 * @file mem.c
 * @brief Memory control system call source file.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @date 2011-06-19
 */

__inline__ void memsetw(void *dest,
							   unsigned short value,
							   unsigned int count) {
  int d0, d1;
  __asm__ __volatile__ ("cld      ;"
						"rep stosl;"
						: "=&D" (d0), "=&c" (d1), "=&a" (value)
						: "0" ((long) dest), "1" (count), "2" (value)
						: "memory");
}

__inline__ void *memcopy(void *dest,
								const void *src,
								unsigned int n) {
  int d0, d1, d2;
  __asm__ __volatile__ ("cld       ;"
						"rep movsl ;"
						"test 2, %4;"
						"je 1f     ;"
						"movsw     ;"
						"1:        ;"
						"test 1, %4;"
						"je 2f     ;"
						"movsb     ;"
						"2:        ;"
						: "=&c" (d0), "=&D" (d1), "=&S" (d2)
						: "0" (n/4), "q" (n), "1" ((long) dest), "2" ((long) src)
						: "memory");
  return dest;
}

unsigned int test_memory(unsigned int start, unsigned int end) {
  char flg486 = 0;
  unsigned int eflg, cr0,i;

  eflg = load_eflags();
  eflg |= EFLAGS_AC_BIT;
  store_eflags(eflg);

  eflg = load_eflags();
  if((eflg & EFLAGS_AC_BIT) != 0) {
	flg486 = 1;
  }
  eflg &= ~EFLAGS_AC_BIT;
  store_eflags(eflg);

  if(flg486 != 0) {
	cr0 = load_cr0();
	cr0 |= CR0_CACHE_DISABLE;
	store_cr0(cr0);
  }

  // check memory.
  i = test_memory_imp(start, end);

  if(flg486 != 0) {
	cr0 = load_cr0();
	cr0 &= ~CR0_CACHE_DISABLE;
	store_cr0(cr0);
  }

  return i;
}

unsigned int test_memory_imp(unsigned int start, unsigned int end) {
  unsigned int i, *p, temp, check_bin1, check_bin2;
  temp = 0;
  check_bin1 = 0x00ff00ff;
  check_bin2 = 0xff00ff00;

  for(i = start; i <= end; i += 0x1000) {
	p = (unsigned int *) (i + 0xffc);
	temp = *p;
	*p = check_bin1;
	*p ^= 0xffffffff;
	if(*p != check_bin2) {
	  *p = temp;
	  break;
	}
	*p = temp;
  }
  
  return i;
}
