#ifndef FAT_H
#define FAT_H

/**
 * @file fat.h
 * @brief 
 * @author Toshimune Tanaka<t-toshi@j.email.ne.jp>
 * @date Sun, 25 Nov, 2012
 */

#include <sys/types.h>
#include "driver_ata.h"

int read_masterbootrecord(u_int16_t pbp[SECTOR_SIZE]);
int read_sector_fromFAT(u_int16_t FatNumber,
						u_int16_t sector[SECTOR_SIZE],
						u_int16_t count);

#endif
