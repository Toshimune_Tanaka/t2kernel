#ifndef MEM_H
#define MEM_H

/**
 * @file mem.h
 * @brief Memory control system call header file.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @date 2011-02-20
 */

/**
 * Segment Selectors
 */
#define KERNEL_CODE 0x08
#define KERNEL_DATA 0x10

/**
 * EFLAGS AC Bit.
 */
#define EFLAGS_AC_BIT 0x00040000

/**
 * disable CR0 cache.
 */
#define CR0_CACHE_DISABLE 0x60000000

/**
 * Descriptor Types
 */
#define INT_GATE 0x8e

/**
 * @brief set memory. size of wide.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @param *dest
 * @param value
 * @param count
 * @return -
 */
__inline__ void memsetw(void *dest,
							   unsigned short value,
							   unsigned int count);

/**
 * @brief copy memory.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @param *dest
 * @param *src
 * @param n
 * @return dest
 */
__inline__ void *memcopy(void *dest,
								const void *src,
								unsigned int n);

/**
 * @brief memory tester main routin.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @param start
 * @param end
 * @return unsigned int
 */
unsigned int test_memory(unsigned int start, unsigned int end);

/**
 * @brief memory tester.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @param start
 * @param end
 * @return unsigned int
 */
unsigned int test_memory_imp(unsigned int start, unsigned int end);

#endif
