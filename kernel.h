#ifndef KERNEL_H
#define KERNEL_H

/**
 * @file kernel.h
 * @brief T2-Kernel(Original OS) header file.
 * @author Toshimune Tanaka(t-toshi@j.email.ne.jp)
 * @date 2011-02-20
 */

/**
 * TaskStateSegment 32bit.
 */
struct TaskStateSegment_32 {
  int backlink;
  int esp0;
  int ss0;
  int esp1;
  int ss1;
  int esp2;
  int ss2;
  int cr3;
  int eip;
  int eflags;
  int eax;
  int ecx;
  int edx;
  int ebx;
  int esp;
  int ebp;
  int esi;
  int edi;
  int es;
  int cs;
  int ss;
  int ds;
  int fs;
  int gs;
  int ldtr;
  int iomap;
};

#endif
